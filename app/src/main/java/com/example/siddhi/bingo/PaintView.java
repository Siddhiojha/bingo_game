package com.example.siddhi.bingo;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

/**
 * Created by Siddhi on 3/23/2018.
 */

public class PaintView extends View implements View.OnTouchListener {
    Paint mPaint;
    float mx;
    float my;
    public PaintView(Context context, @Nullable AttributeSet attrs) {
        super( context, attrs );

        mPaint = new Paint(  );
        mx = my = 100;
    }
    @Override
    protected  void onDraw(Canvas canvas){
        super.onDraw( canvas );
        mPaint.setColor( Color.BLUE );
        canvas.drawCircle( mx,my,50,mPaint );
        invalidate();
    }
    @Override
    public boolean onTouch(View v, MotionEvent event) {
       switch (event.getAction()){
           case MotionEvent.ACTION_DOWN:
               mx=event.getX();
               my=event.getY();
       }
        return true;
    }
}
