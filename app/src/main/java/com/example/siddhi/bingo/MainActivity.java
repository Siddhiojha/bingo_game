package com.example.siddhi.bingo;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Random;

import com.example.siddhi.bingo.R;

public class MainActivity extends AppCompatActivity {
    int bingo = 0, newbingo = 0;
    Random random = new Random();
    TextView bingoarray;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate( savedInstanceState );
        setContentView( R.layout.activity_main);
        int i, count = 0, flag = 0;
        random_integer();
        newbingo = random.nextInt( 99 );
        String check = Integer.toString( newbingo );
        TextView output = (TextView) findViewById( R.id.result );
        output.setText( check );
        PaintView paintView = (PaintView) findViewById( R.id.paintview );
        paintView.setOnTouchListener( paintView );
    }

    /**
     * count=count+1;
     * String newresult=Integer.toString( newbingo );
     * TextView newoutput = (TextView) findViewById(R.id.result);
     * newoutput.setText( newresult );
     **/

    private void random_integer() {
        int i, flag = 0;
        for (i = 1; i < 25; i++) {
            switch (i) {
                case 1:
                    bingo = random.nextInt( 99 );
                    if (newbingo == bingo) {
                        flag = 1;
                    }
                    TextView bingoarray2 = (TextView) findViewById(R.id.num1 );
                    String result2 = Integer.toString( bingo );
                    bingoarray2.setText( result2 );
                case 2:
                    bingo = random.nextInt( 99 );
                    TextView bingoarray3 = (TextView) findViewById(R.id.num2 );
                    String result3 = Integer.toString( bingo );
                    bingoarray3.setText( result3 );
                case 3:
                    bingo = random.nextInt( 99 );
                    TextView bingoarray4 = (TextView) findViewById( R.id.num3 );
                    String result4 = Integer.toString( bingo );
                    bingoarray4.setText( result4 );
                case 4:
                    bingo = random.nextInt( 99 );
                    TextView bingoarray5 = (TextView) findViewById( R.id.num4 );
                    String result5 = Integer.toString( bingo );
                    bingoarray5.setText( result5 );
                case 5:
                    bingo = random.nextInt( 99 );
                    TextView bingoarray6 = (TextView) findViewById(R.id.num5 );
                    String result6 = Integer.toString( bingo );
                    bingoarray6.setText( result6 );
                case 6:
                    bingo = random.nextInt( 99 );
                    TextView bingoarray7 = (TextView) findViewById(R.id.num6 );
                    String result7 = Integer.toString( bingo );
                    bingoarray7.setText( result7 );
                case 7:
                    bingo = random.nextInt( 99 );
                    TextView bingoarray8 = (TextView) findViewById(R.id.num7 );
                    String result8 = Integer.toString( bingo );
                    bingoarray8.setText( result8 );
                case 8:
                    bingo = random.nextInt( 99 );
                    TextView bingoarray9 = (TextView) findViewById( R.id.num8 );
                    String result9 = Integer.toString( bingo );
                    bingoarray9.setText( result9 );
                case 9:
                    bingo = random.nextInt( 99 );
                    TextView bingoarray10 = (TextView) findViewById( R.id.num9 );
                    String result10 = Integer.toString( bingo );
                    bingoarray10.setText( result10 );
                case 10:
                    bingo = random.nextInt( 99 );
                    TextView bingoarray11 = (TextView) findViewById( R.id.num10 );
                    String result11 = Integer.toString( bingo );
                    bingoarray11.setText( result11 );
                case 11:
                    bingo = random.nextInt( 99 );
                    TextView bingoarray12 = (TextView) findViewById( R.id.num11 );
                    String result12 = Integer.toString( bingo );
                    bingoarray12.setText( result12 );
                case 12:
                    bingo = random.nextInt( 99 );
                    TextView bingoarray13 = (TextView) findViewById( R.id.num12 );
                    String result13 = Integer.toString( bingo );
                    bingoarray13.setText( result13 );
                case 13:
                    bingo = random.nextInt( 99 );
                    TextView bingoarray14 = (TextView) findViewById( R.id.num13 );
                    String result14 = Integer.toString( bingo );
                    bingoarray14.setText( result14 );
                case 14:
                    bingo = random.nextInt( 99 );
                    TextView bingoarray15 = (TextView) findViewById( R.id.num14 );
                    String result15 = Integer.toString( bingo );
                    bingoarray15.setText( result15 );
                case 15:
                    bingo = random.nextInt( 99 );
                    TextView bingoarray16 = (TextView) findViewById( R.id.num15 );
                    String result16 = Integer.toString( bingo );
                    bingoarray16.setText( result16 );
                case 16:
                    bingo = random.nextInt( 99 );
                    TextView bingoarray17 = (TextView) findViewById( R.id.num16 );
                    String result17 = Integer.toString( bingo );
                    bingoarray17.setText( result17 );
                case 17:
                    bingo = random.nextInt( 99 );
                    TextView bingoarray18 = (TextView) findViewById( R.id.num17 );
                    String result18 = Integer.toString( bingo );
                    bingoarray18.setText( result18 );
                case 18:
                    bingo = random.nextInt( 99 );
                    TextView bingoarray19 = (TextView) findViewById( R.id.num18 );
                    String result19 = Integer.toString( bingo );
                    bingoarray19.setText( result19 );
                case 19:
                    bingo = random.nextInt( 99 );
                    TextView bingoarray20 = (TextView) findViewById( R.id.num19 );
                    String result20 = Integer.toString( bingo );
                    bingoarray20.setText( result20 );
                case 20:
                    bingo = random.nextInt( 99 );
                    TextView bingoarray21 = (TextView) findViewById( R.id.num20 );
                    String result21 = Integer.toString( bingo );
                    bingoarray21.setText( result21 );
                case 21:
                    bingo = random.nextInt( 99 );
                    TextView bingoarray22 = (TextView) findViewById( R.id.num21 );
                    String result22 = Integer.toString( bingo );
                    bingoarray22.setText( result22 );
                case 22:
                    bingo = random.nextInt( 99 );
                    TextView bingoarray23 = (TextView) findViewById( R.id.num22 );
                    String result23 = Integer.toString( bingo );
                    bingoarray23.setText( result23 );
                case 23:
                    bingo = random.nextInt( 99 );
                    TextView bingoarray24 = (TextView) findViewById( R.id.num23 );
                    String result24 = Integer.toString( bingo );
                    bingoarray24.setText( result24 );
                case 24:
                    bingo = random.nextInt( 99 );
                    TextView bingoarray25 = (TextView) findViewById( R.id.num24 );
                    String result25 = Integer.toString( bingo );
                    bingoarray25.setText( result25 );
                case 25:
                    bingo = random.nextInt( 99 );
                    TextView bingoarray26 = (TextView) findViewById( R.id.num25 );
                    String result26 = Integer.toString( bingo );
                    bingoarray26.setText( result26 );


            }
        }
    }
}


